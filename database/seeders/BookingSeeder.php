<?php

namespace Database\Seeders;

use App\Models\Booking;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Seeder;

class BookingSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$bookings = [
			[
				'room_id' => 1,
				'check_in_at' => '2021-02-01',
				'check_out_at' => '2021-02-05',
			],
			[
				'room_id' => 1,
				'check_in_at' => '2021-02-10',
				'check_out_at' => '2021-02-16',
			],
			[
				'room_id' => 2,
				'check_in_at' => '2021-02-8',
				'check_out_at' => '2021-02-16',
			],
			[
				'room_id' => 3,
				'check_in_at' => '2021-02-08',
				'check_out_at' => '2021-02-16',
			],
			[
				'room_id' => 3,
				'check_in_at' => '2021-02-22',
				'check_out_at' => '2021-02-26',
			],
			[
				'room_id' => 4,
				'check_in_at' => '2021-02-12',
				'check_out_at' => '2021-02-16',
			],
			[
				'room_id' => 5,
				'check_in_at' => '2021-02-11',
				'check_out_at' => '2021-02-18',
			],
			[
				'room_id' => 6,
				'check_in_at' => '2021-02-14',
				'check_out_at' => '2021-02-19',
			],
			[
				'room_id' => 6,
				'check_in_at' => '2021-02-01',
				'check_out_at' => '2021-02-05',
			],
			[
				'room_id' => 7,
				'check_in_at' => '2021-02-14',
				'check_out_at' => '2021-02-18',
			],
			[
				'room_id' => 8,
				'check_in_at' => '2021-02-09',
				'check_out_at' => '2021-02-16',
			],
			[
				'room_id' => 9,
				'check_in_at' => '2021-02-20',
				'check_out_at' => '2021-02-25',
			],
			[
				'room_id' => 9,
				'check_in_at' => '2021-02-14',
				'check_out_at' => '2021-02-19',
			],
			[
				'room_id' => 10,
				'check_in_at' => '2021-02-12',
				'check_out_at' => '2021-02-16',
			],
		];

		Booking::factory()->count(count($bookings))->state(new Sequence(...$bookings))->create();
	}
}
