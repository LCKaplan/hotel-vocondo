<?php

namespace Database\Seeders;

use App\Models\Room;
use Illuminate\Database\Seeder;

class RoomSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Room::factory()->count(7)->create(['room_type_id' => 1]);
		Room::factory()->count(3)->create(['room_type_id' => 2]);
	}
}
