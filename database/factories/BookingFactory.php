<?php

namespace Database\Factories;

use App\Models\Booking;
use App\Models\Room;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class BookingFactory extends Factory
{
	/**
	 * The name of the factory's corresponding model.
	 *
	 * @var string
	 */
	protected $model = Booking::class;

	/**
	 * Define the model's default state.
	 *
	 * @return array
	 */
	public function definition()
	{
		$ref = $this->faker->dateTimeBetween($startDate = 'now', $endDate = '+3 months');
		$check_in = Carbon::parse($ref);
		$check_out = Carbon::parse($ref)->addDays(rand(5, 10));

		return [
			'room_id' => Room::factory(),
			'check_in_at' => $check_in->format('Y-m-d'),
			'check_out_at' => $check_out->format('Y-m-d'),
		];
	}
}
