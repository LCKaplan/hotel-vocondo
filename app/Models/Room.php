<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
	use HasFactory;

	public function roomType()
	{
		return $this->belongsTo(RoomType::class);
	}

	public function bookings()
	{
		return $this->hasMany(Booking::class);
	}

	/**
	 * Check if the room is available for given date;
	 * 
	 * @param \Carbon\Carbon $date
	 * 
	 * @return bool
	 */
	public function isAvailable(Carbon $date)
	{
		return !$this->bookings->contains(function ($booking) use ($date) {
			return $date >= $booking->check_in_at  && $date < $booking->check_out_at;
		});
	}

	/**
	 * Retrieve all rooms with bookings in given period
	 * 
	 * @param  \Illuminate\Database\Eloquent\Builder $query
	 * @param \Carbon\Carbon|string $start_date
	 * @param \Carbon\Carbon|string $end_date
	 * 
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public static function scopeWithBookingsBetween($query, $start_date, $end_date)
	{
		return $query->with(['bookings' => function ($query) use ($start_date, $end_date) {
			$query
				->whereBetween('check_in_at', [$start_date, $end_date])
				->orWhereBetween('check_out_at', [$start_date, $end_date])
				->orWhere(function ($q) use ($start_date, $end_date) {
					$q->where('check_in_at', '<', $start_date)
						->where('check_out_at', '>', $end_date);
				});
		}]);
	}
}
