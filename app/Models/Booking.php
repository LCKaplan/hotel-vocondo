<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
	use HasFactory;

	/**
	 * The attributes that should be cast.
	 *
	 * @var array
	 */
	protected $casts = [
		'check_in_at' => 'date',
		'check_out_at' => 'date',
	];

	public function room()
	{
		return $this->belongsTo(Room::class);
	}
}
