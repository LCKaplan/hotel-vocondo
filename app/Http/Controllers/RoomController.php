<?php

namespace App\Http\Controllers;

use App\Http\Resources\RoomResource;
use App\Models\Room;

class RoomController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
	 */
	public function index()
	{
		return RoomResource::collection(Room::with('roomType')->get());
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Models\Room  $room
	 * @return \App\Http\Resources\RoomResource
	 */
	public function show(Room $room)
	{
		return new RoomResource($room->load('roomType'));
	}
}
