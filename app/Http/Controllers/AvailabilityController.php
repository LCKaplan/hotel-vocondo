<?php

namespace App\Http\Controllers;

use App\Models\Room;
use App\Rules\GreaterThenDate;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class AvailabilityController extends Controller
{
	/**
	 * Display given period with room availabilities
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		$this->validate($request, [
			'start_date' => ['required', 'date:Y-m-d'],
			'end_date' => ['required', 'date:Y-m-d', new GreaterThenDate($request->start_date)],
		]);

		$roomTypes = Room::withBookingsBetween($request->start_date, $request->end_date)
			->with('roomType')
			->get()
			->groupBy('roomType.name');

		$searchPeriod = collect(new CarbonPeriod($request->start_date, $request->end_date));

		$availablities = $searchPeriod->map(function ($date) use ($roomTypes) {
			//Count available rooms for each room type;
			$data = $roomTypes->mapWithKeys(function ($rooms, $roomType) use ($date) {
				$availableRoomCount = $rooms->filter(function ($room) use ($date) {
					return $room->isAvailable($date);
				})->count();

				return [$roomType => $availableRoomCount];
			});

			$data['available'] = (bool) $data->sum();
			$data['date'] = $date->format('Y-m-d');

			return $data;
		});

		return Response::json(['data' => $availablities]);
	}
}
