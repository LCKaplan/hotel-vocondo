<?php

namespace App\Rules;

use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;

class GreaterThenDate implements Rule
{
	protected $givenDate;
	/**
	 * Create a new rule instance.
	 * 
	 * @param string $date
	 * 
	 * @return void
	 */
	public function __construct($date)
	{
		$this->givenDate = Carbon::parse($date);
	}

	/**
	 * Determine if the validation rule passes.
	 *
	 * @param  string  $attribute
	 * @param  mixed  $value
	 * @return bool
	 */
	public function passes($attribute, $value)
	{
		return Carbon::parse($value)->greaterThan($this->givenDate);
	}

	/**
	 * Get the validation error message.
	 *
	 * @return string
	 */
	public function message()
	{
		return 'The :attribute must be greater than ' . $this->givenDate->format('Y-m-d') . '.';
	}
}
