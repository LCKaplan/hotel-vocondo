-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Feb 11, 2021 at 09:04 PM
-- Server version: 5.7.26
-- PHP Version: 7.4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hotel-vocondo`
--

-- --------------------------------------------------------

--
-- Table structure for table `bookings`
--

DROP TABLE IF EXISTS `bookings`;
CREATE TABLE IF NOT EXISTS `bookings` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `room_id` bigint(20) UNSIGNED NOT NULL,
  `check_in_at` date NOT NULL,
  `check_out_at` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bookings`
--

INSERT INTO `bookings` (`id`, `room_id`, `check_in_at`, `check_out_at`, `created_at`, `updated_at`) VALUES
(1, 1, '2021-02-01', '2021-02-05', '2021-02-11 18:22:08', '2021-02-11 18:22:08'),
(2, 1, '2021-02-10', '2021-02-16', '2021-02-11 18:22:08', '2021-02-11 18:22:08'),
(3, 2, '2021-02-08', '2021-02-16', '2021-02-11 18:22:08', '2021-02-11 18:22:08'),
(4, 3, '2021-02-08', '2021-02-16', '2021-02-11 18:22:08', '2021-02-11 18:22:08'),
(5, 3, '2021-02-22', '2021-02-26', '2021-02-11 18:22:08', '2021-02-11 18:22:08'),
(6, 4, '2021-02-12', '2021-02-16', '2021-02-11 18:22:08', '2021-02-11 18:22:08'),
(7, 5, '2021-02-11', '2021-02-18', '2021-02-11 18:22:08', '2021-02-11 18:22:08'),
(8, 6, '2021-02-14', '2021-02-19', '2021-02-11 18:22:08', '2021-02-11 18:22:08'),
(9, 6, '2021-02-01', '2021-02-05', '2021-02-11 18:22:08', '2021-02-11 18:22:08'),
(10, 7, '2021-02-14', '2021-02-18', '2021-02-11 18:22:08', '2021-02-11 18:22:08'),
(11, 8, '2021-02-09', '2021-02-16', '2021-02-11 18:22:08', '2021-02-11 18:22:08'),
(12, 9, '2021-02-20', '2021-02-25', '2021-02-11 18:22:08', '2021-02-11 18:22:08'),
(13, 9, '2021-02-14', '2021-02-19', '2021-02-11 18:22:08', '2021-02-11 18:22:08'),
(14, 10, '2021-02-12', '2021-02-16', '2021-02-11 18:22:08', '2021-02-11 18:22:08');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_02_11_094747_create_rooms_table', 1),
(5, '2021_02_11_095015_create_room_types_table', 1),
(6, '2021_02_11_101216_create_bookings_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

DROP TABLE IF EXISTS `rooms`;
CREATE TABLE IF NOT EXISTS `rooms` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `room_type_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`id`, `name`, `room_type_id`, `created_at`, `updated_at`) VALUES
(1, 'facilis', 1, '2021-02-11 18:22:08', '2021-02-11 18:22:08'),
(2, 'assumenda', 1, '2021-02-11 18:22:08', '2021-02-11 18:22:08'),
(3, 'magni', 1, '2021-02-11 18:22:08', '2021-02-11 18:22:08'),
(4, 'error', 1, '2021-02-11 18:22:08', '2021-02-11 18:22:08'),
(5, 'maiores', 1, '2021-02-11 18:22:08', '2021-02-11 18:22:08'),
(6, 'dolores', 1, '2021-02-11 18:22:08', '2021-02-11 18:22:08'),
(7, 'quasi', 1, '2021-02-11 18:22:08', '2021-02-11 18:22:08'),
(8, 'nam', 2, '2021-02-11 18:22:08', '2021-02-11 18:22:08'),
(9, 'quos', 2, '2021-02-11 18:22:08', '2021-02-11 18:22:08'),
(10, 'consectetur', 2, '2021-02-11 18:22:08', '2021-02-11 18:22:08');

-- --------------------------------------------------------

--
-- Table structure for table `room_types`
--

DROP TABLE IF EXISTS `room_types`;
CREATE TABLE IF NOT EXISTS `room_types` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `room_types`
--

INSERT INTO `room_types` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Double Room', 'Veritatis et qui harum modi.', '2021-02-11 18:22:08', '2021-02-11 18:22:08'),
(2, 'Triple Room', 'Hic sed sequi cum fugit aliquid similique odio.', '2021-02-11 18:22:08', '2021-02-11 18:22:08');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
