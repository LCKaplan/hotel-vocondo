## How to setup

-   Install dependencies by running `composer install`.
-   Create a `.env` file on project root by copying the .env.example file.
-   Set DB_DATABASE, DB_USERNAME, DB_PASSWORD and other related fields if necessary.
-   Create the database with the name used in DB_DATABASE field.
-   Run the following commands to generate app key, create database tables and seed them with data:
    -   `php artisan key:generate`
    -   `php artisan migrate --seed`
-   Run `php artisan serve` to start development server.

PS: Incase something goes wrong, hotel-vocondo.sql file can be found at the project root.

### API Endpoints

-   `/api/rooms`: Lists all the rooms in the hotel.
-   `/api/rooms/{room_id}`: Get details of a room with its id.
-   `/api/availablity`: Get a list of days with available rooms by room types and general availability. Following parameters are required:
    -   `start_date`: beggining of requested period. Format: `Y-m-d`
    -   `end_date`: end of requested period. Format: `Y-m-d`

PS: Bookings are in February.
